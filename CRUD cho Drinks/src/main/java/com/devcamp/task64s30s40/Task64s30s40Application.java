package com.devcamp.task64s30s40;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task64s30s40Application {

	public static void main(String[] args) {
		SpringApplication.run(Task64s30s40Application.class, args);
	}

}
