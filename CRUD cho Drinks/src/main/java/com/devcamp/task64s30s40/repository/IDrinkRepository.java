package com.devcamp.task64s30s40.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task64s30s40.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {
    CDrink findById(long id);
}
