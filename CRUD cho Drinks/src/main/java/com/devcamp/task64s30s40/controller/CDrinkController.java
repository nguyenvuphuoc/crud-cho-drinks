package com.devcamp.task64s30s40.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task64s30s40.model.CDrink;
import com.devcamp.task64s30s40.repository.IDrinkRepository;

@CrossOrigin
@RestController
public class CDrinkController {

    @Autowired
    IDrinkRepository pDrinkRepository;

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks(){
        try{
            List<CDrink> pCDrinks = new ArrayList<>();

            pDrinkRepository.findAll().forEach(pCDrinks::add);

            return new ResponseEntity<>(pCDrinks, HttpStatus.OK);
        } catch (Exception e){
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/drinks/{id}")
    public ResponseEntity<CDrink> getDrinkById(@PathVariable("id") long id){
        try{
            CDrink drink = pDrinkRepository.findById(id);

            return new ResponseEntity<>(drink, HttpStatus.OK);
        } catch(Exception e){
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/drinks")
    public ResponseEntity<Object> createDrink(@Valid @RequestBody CDrink pCDrink){
        try {
            pCDrink.setNgayTao(new Date());
            pCDrink.setNgayCapNhap(null);
            CDrink _drinks = pDrinkRepository.save(pCDrink);
            return new ResponseEntity<>(_drinks, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity().body("Failed to Create specified Drink: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/drinks/{id}")
    public ResponseEntity<Object> updateDrink(@PathVariable("id") long id, @RequestBody CDrink pCDrink){
    try {
        CDrink drinkData = pDrinkRepository.findById(id);
        CDrink drink = drinkData;
        drink.setMaNuocUong(pCDrink.getMaNuocUong());
        drink.setTenNuocUong(pCDrink.getTenNuocUong());
        drink.setDonGia(pCDrink.getDonGia());
        drink.setGhiChu(pCDrink.getGhiChu());
        drink.setNgayCapNhap(new Date());
        try {
            return new ResponseEntity<>(pDrinkRepository.save(drink), HttpStatus.OK);
        } catch (Exception e) {
            return ResponseEntity.unprocessableEntity().body("Failed to Update specified Drink: " + e.getCause().getCause().getMessage());
        }
    } catch (Exception e) {
        System.out.println(e);
        return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
    }

    @DeleteMapping("/drinks/{id}")
    public ResponseEntity<CDrink> deleteDrink(@PathVariable("id") long id){
        try {
            pDrinkRepository.deleteById(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
